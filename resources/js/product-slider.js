document.addEventListener('DOMContentLoaded', function() {
    var secondarySlider = new Splide('#secondary-slider', {
        fixedWidth: 43,
        height: 43,
        gap: 0,
        pagination: false,
        cover: true,
        arrows: false,
        isNavigation: true,

        breakpoints: {
            '600': {
                fixedWidth: 43,
                height: 43,
            }
        },
    }).mount();

    var primarySlider = new Splide('#primary-slider', {
        fixedWidth: 350,
        height: 275,
        type: 'fade',
        heightRatio: 0.5,
        pagination: false,
        arrows: false,
        cover: true,
    }); // do not call mount() here.

    primarySlider.sync(secondarySlider).mount();
});